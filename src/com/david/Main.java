package com.david;

public class Main {


    public static void main(String[] args) {

        //Ex1
        MegaBytesConverter megaBytesConverter = new MegaBytesConverter();
        megaBytesConverter.printMegaBytesAndKiloBytes(-1000);

        //Ex2
        BarkingDog barkingDog = new BarkingDog();
        System.out.println(barkingDog.bark(true,9));

        //Ex3
        System.out.println(LeapYear.isLeapYear(2000));

        //Ex4
        System.out.println(DecimalComparator.areEqualByThreeDecimalPlaces(3.175,3.1757));

        //Ex5
        System.out.println(EqualSumChecker.hasEqualSum(1,4,5));

        //Ex6
        System.out.println(TeenNumberChecker.hasTeen(22,23,24));

        //Ex7
        System.out.println(AreaCalculator.area(-1.0,4.0));

        //Ex8
        MinutesToYearsDaysCalculator.printYearsAndDays(561600);

        //Ex9
        IntEqualityPrinter.printEqual(1,2,3);

        //Ex10
        System.out.println(PlayingCat.isCatPlaying(false,35));

        //Ex14
        System.out.println(NumberPalindrome.isPalindrome(11212));

        //Ex15
        System.out.println(FirstLastDigitSum.sumFirstAndLastDigit(135232342));

        //Ex16
        System.out.println(EvenDigitSum.getEvenDigitSum(0));

        //Ex17
        System.out.println(SharedDigit.hasSharedDigit(15,55));

        //Ex18
        System.out.println(LastDigitCheck.hasSameLastDigit(9,99,999));
    }

    public static class MegaBytesConverter {

        public void printMegaBytesAndKiloBytes(int kiloBytes) {
            if(kiloBytes < 0) {
                System.out.println("Invalid Value");
            } else {

                int mbValue = kiloBytes / 1024;
                int oddValue = kiloBytes % 1024;
                System.out.println(kiloBytes + " KB" + " = " + mbValue + " MB "
                        + "and " + oddValue + " KB ");
            }
        }
    }

    public static class BarkingDog {
        public boolean bark (boolean barking, int hourOfDay) {
            if (hourOfDay < 0 || hourOfDay > 23) {
                return false;
            } else if (barking == true) {

                if (hourOfDay < 8 && hourOfDay > 0) {
                    return true;
                } else if (hourOfDay > 22) {
                    return true;
                } else {
                    return false;
                }
            }  else if (barking == false) {
                return false;
            }
            return false;
        }
    }

    public static class LeapYear{

        public static boolean isLeapYear(int year){
            if (year < 1 || year > 9999){
                return false;
            } else {
                if (year % 4 == 0 && (year % 100 != 0 || year % 400 ==0)){
                    System.out.println("This is a leap year");
                    return true;
                } else {
                    System.out.println("This is not a leap year");
                    return false;
                }

            }

        }

    }

    public static class DecimalComparator{
        public static boolean areEqualByThreeDecimalPlaces(double num1, double num2){
           return (int)(num1 * 1000) == (int)(num2 * 1000);
        }
    }

    public static class EqualSumChecker{
        public static boolean hasEqualSum(int num1, int num2, int num3){
            return (num1 + num2) == num3;
        }

    }

    public static class TeenNumberChecker {
        public static boolean hasTeen(int num1,int num2,int num3){
            return (num1 >= 13 && num1 <=19) || (num2 >= 13 && num2 <=19)
                    || (num3 >= 13 && num3 <=19);
        }
    }

    public static class AreaCalculator{
        public static double area (double radius){
            if (radius < 0) {
                return -1.0;
            }
            return (radius*radius) * Math.PI;
        }

        public static double area (double x, double y){
            if (x < 0 || y <0) {
                return -1.0;
            }
            return x * y;
        }
    }

    public static class MinutesToYearsDaysCalculator{
        public static void printYearsAndDays(long minutes){
            if (minutes < 0) {
                System.out.println("Invalid Value");
            }

            System.out.println(minutes + " min" + " ="+ (minutes/525600)
                                + " y "+ (minutes%525600)/1440 +" d");
        }
    }

    public static class IntEqualityPrinter{
        public static void printEqual(int num1, int num2, int num3){
            if( num1 < 0 || num2 < 0 || num3 < 0) {
                System.out.println("Invalid Value");
            }else if (num1 == num2 && num2 == num3) {
                System.out.println("All numbers are equal");
            }else if (num1 != num2 && num2 != num3 && num1 != num3){
                System.out.println("All numbers are different");
            }else System.out.println("Neither all are equal or different");
        }
    }

    public static class PlayingCat{
        public static boolean isCatPlaying(boolean summer,int temp){
            if (summer) {
                if (temp < 25 || temp > 45){
                    return false;
                } else return true;
            } else {
                if (temp < 25 || temp > 35){
                    return false;
                } else return true;
            }
        }
    }

    public static class NumberPalindrome{
        public static boolean isPalindrome(int number){
            int palindrome = number;
            int reverse = 0;
            while (palindrome != 0){
                int lastDigit = palindrome % 10;
                reverse = reverse * 10 + lastDigit;
                palindrome = palindrome / 10;
            }

            if (number == reverse){
                return true;
            } else {
                return false;
            }
        }
    }

    public static class FirstLastDigitSum{
        public static int sumFirstAndLastDigit(int number) {
            if (number < 0) {
                return -1;
            }

            int firstDig = 0;
            int lastDig = number % 10;

            while (number != 0) {
                firstDig = number % 10;
                number /= 10;
            }

            return firstDig + lastDig;
        }
    }

    public static class EvenDigitSum{
        public static int getEvenDigitSum(int number){
            int evenNum = 0;
            int sum = 0;

            if (number < 0){
                return -1;
            }

            while (number != 0) {
                evenNum = number % 10;
                number /= 10;
                if (evenNum % 2 == 0) {
                    sum += evenNum;
                }
            }

            return sum;
        }
    }

    public static class SharedDigit{
        public static boolean hasSharedDigit(int num1, int num2){
            int digit,digit2 = 0;

            if (num1 < 10 || num1 > 99 || num2 < 10 || num2 > 99 ){
                return false;
            }

            while (num1 != 0){
                digit = num1 % 10;
                num1 /= 10;
                while (num2 != 0) {
                    digit2 = num2 % 10;
                    num2 /= 10;
                    if (digit2 == digit){
                        return true;
                    }
                }
            }

            return false;
        }
    }

    public static class LastDigitCheck{
        public static boolean hasSameLastDigit(int num1, int num2, int num3){
            int lastDig1, lastDig2,lastDig3 = 0;

            if (num1 < 10 || num1 > 1000 || num2 < 10 || num2 > 1000 || num3 < 10 || num3 > 10000){
                return false;
            }

            lastDig1 = num1 % 10;
            lastDig2 = num2 % 10;
            lastDig3 = num3 % 10;

            if (lastDig1 == lastDig2 || lastDig1 == lastDig3 || lastDig2 == lastDig3) {
                return true;
            }

            return false;
        }
    }
}
